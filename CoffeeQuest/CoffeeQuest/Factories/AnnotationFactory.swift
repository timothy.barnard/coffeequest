//
//  AnnotationFactory.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import UIKit
import MapKit

/// Factory to build annotations
public class AnnotationFactory {
    
    public func createBusinessMapViewModel(for business: Business) -> BusinessMapViewModel {
        
        let name = business.name
        let rating = business.rating
        let coordinate = business.location
        let image: UIImage
        
        switch rating {
        case 0.0..<3.0:
            image = UIImage(named: "terrible")!
        case 3.0..<3.5:
            image = UIImage(named: "bad")!
        case 3.5..<4.0:
            image = UIImage(named: "meh")!
        case 4.0..<4.75:
            image = UIImage(named: "good")!
        case 4.75..<5.0:
            image = UIImage(named: "great")!
        default:
            image = UIImage(named: "bad")!
        }
        
        return .init(coordinate: coordinate, name: name, rating: rating, image: image)
    }
}
