//
//  YLPClient+BusinessSearchClient.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation
import MapKit
import Combine

/// Yelp service extension for business search client protocol
extension YelpService: BusinessSearchClient {
    
    func search(with coordinate: CLLocationCoordinate2D, term: String, limit: UInt, offset: UInt) -> AnyPublisher<[Business], Error> {
        let coordinate: Coordinate = .init(
            latitude: coordinate.latitude,
            longitude: coordinate.longitude)
        let api: API = .search(
            coordinate: coordinate,
            term: term,
            limit: limit,
            offset: offset,
            sort: "")
        return self.makeRequest(api)
            .map { $0.businesses.adaptToBusinesses() }
            .eraseToAnyPublisher()
    }
}

fileprivate extension Array where Element == YelpBusiness {
    
    
    /// Converts `[YelpBusiness]` to `[Business]`
    /// - Returns: `[Business]`
    func adaptToBusinesses() -> [Business] {
        return self.compactMap { yelpBusiness in
            let location = CLLocationCoordinate2D(
                latitude: yelpBusiness.coordinates.latitude,
                longitude: yelpBusiness.coordinates.longitude)
            return Business(name: yelpBusiness.name, rating: yelpBusiness.rating, location: location)
        }
    }
}
