//
//  Business.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation
import MapKit

public struct Business {
    var name: String
    var rating: Double
    var location: CLLocationCoordinate2D
}
