//
//  Filter.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 10/09/2021.
//

import Foundation


/// Filter sequence to filter over businesses
public struct Filter {
    public let filter: (Business) -> Bool
    public var businesses: [Business]
    
    ///A way to instantiate the class
    public static func identity() -> Filter {
        return Filter(filter: { _ in true }, businesses: [])
    }
    
    /// Filter by min star rating
    /// - Parameter starRating: `Double`
    /// - Returns: `Filter`
    public static func starRating(atLeast starRating: Double) -> Filter {
        return Filter(filter: { $0.rating >= starRating }, businesses: [])
    }
    
    /// Get a list of business with the filter provided
    /// - Returns: `[Business]`
    public func filterBusiness() -> [Business] {
        return businesses.filter (filter)
    }
}

//MARK: - Sequence
extension Filter: Sequence {
    
    public func makeIterator() -> IndexingIterator<[Business]> {
        return filterBusiness().makeIterator()
    }
}
