//
//  BusinessSearchClient.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation
import MapKit
import Combine

/// Protocol for business search client
public protocol BusinessSearchClient {
    
    /// Search for business
    /// - Parameters:
    ///   - coordinate: `CLLocationCoordinate2D`
    ///   - term: `String`
    ///   - limit: `UInt`
    ///   - offset: `UInt`
    func search(
        with coordinate: CLLocationCoordinate2D,
        term: String,
        limit: UInt,
        offset: UInt) -> AnyPublisher<[Business], Error>
}
