//
//  Coodinate.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation

public struct Coordinate {
    public let latitude: Double
    public let longitude: Double
}
