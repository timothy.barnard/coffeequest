//
//  YelpBusiness.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation

public struct YelpBusinesses: Decodable {
    let businesses: [YelpBusiness]
}

public struct YelpBusiness: Decodable {
    let id: String
    let name: String
    let rating: Double
    let coordinates: YelpCoordinate
}

public struct YelpCoordinate: Decodable {
    let latitude: Double
    let longitude: Double
}
