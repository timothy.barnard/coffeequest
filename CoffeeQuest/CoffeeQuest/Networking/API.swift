//
//  API.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation

public enum API {
    
    case search(
            coordinate: Coordinate,
            term: String,
            limit: UInt, offset: UInt,
            sort: String)
}

extension API {
    
    private var host: String {
        "api.yelp.com"
    }
    
    private var path: String {
        "/v3/businesses/search"
    }
    
    private var headers: [String: String] {
        ["Authorization": "Bearer \(YelpAPIKey)"]
    }
    
    private var method: String {
        "GET"
    }
    
    private var url: URL? {
        var urlComponents: URLComponents = .init()
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.scheme = "https"
        urlComponents.queryItems = urlQueryItems
        return urlComponents.url
    }
    
    private var urlQueryItems: [URLQueryItem] {
        switch self {
        case .search(let coordinate, let term, let limit, let offset, _):
            return [
                .init(name: "latitude", value: "\(coordinate.latitude)"),
                .init(name: "longitude", value: "\(coordinate.longitude)"),
                .init(name: "term", value: term),
                .init(name: "limit", value: "\(limit)"),
                .init(name: "offset", value: "\(offset)")
            ]

        }
    }
    
    public var urlRequest: URLRequest? {
        guard let url = self.url else {return nil}
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        return request
    }
}
