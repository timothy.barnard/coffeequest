//
//  YelpService.swift
//  CoffeeQuest
//
//  Created by Timothy Barnard on 09/09/2021.
//

import Foundation
import Combine

/// Service for YELP
class YelpService {
    
    let session: URLSession
    let decoder: JSONDecoder = .init()
    
    init(_ session: URLSession = .shared) {
        self.session = session
    }
    
    /// Make a request with the given API
    /// - Parameter api: `API`
    /// - Returns:  `AnyPublisher<YelpBusinesses, Error>`
    func makeRequest(_ api: API) -> AnyPublisher<YelpBusinesses, Error> {
        guard let request = api.urlRequest else {
            return Fail(error: NSError(domain: "", code: 100, userInfo: nil)).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: request)
            .mapError { $0 }
            .map { $0.data }
            .decode(type: YelpBusinesses.self, decoder: decoder)
            .eraseToAnyPublisher()
    }
}
